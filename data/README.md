# Source de données

| Données | Accès | Source | Lien | Période traitée |
| ------- | ----- | ------ | ---- | --------------- |
| **Indice de défavorisation sociale (FDep) par IRIS** | Par commune | Open Data Soft | https://www.cepidc.inserm.fr/documentation/indicateurs-ecologiques-du-niveau-socio-economique | 2015 |
| **Température (°C)** | Par commune | Historical Weather API, qui se base sur Copernicus | https://open-meteo.com/en/docs/historical-weather-api | Eté 2022 (juin, juillet, août) |
| **Equipement des résidences principales en climatisation** | Par commune |  | | |
| **Couverture végétale (CORINE Land Cover)** | Par parcelle | data.gouv.fr | https://www.data.gouv.fr/fr/datasets/corine-land-cover-occupation-des-sols-en-france/ | 2018 |
| **Découpage administratif communal français** (pour la carte des T°C) | Par commune | data.gouv.fr | https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/ | 2022 | 

# Référentiel des communes (2022)

Nous utilisons le référentiel de 2022 fourni par l'INSEE, qu'on peut télécharger sur [sirse.atlasante.fr](https://sirse.atlasante.fr/#c=externaldata).

Dans le cadre de ce challenge, on s'intéresse seulement à la France métropolitaine. Ainsi, nous avons : 
- Communes : 34 955
- Communes et arrondissements : 34 997