"""
read_excel.py
Function to read an excel file.

"""

import re
import pandas as pd


def read_excel(filepath: str, range: str):
    """Returns an excel sheet as a dataframe.

    Args:
        filepath (str): excel file path
        range (str): cells' range to read, e.g. "C5:G8"

    Returns:
        pd.DataFrame: excel sheet
    """

    cells = range.split(":")
    cols_rows = [re.split(r"(\d+)", s) for s in cells]
    first_col = cols_rows[0][0]
    first_row = int(cols_rows[0][1])
    last_col = cols_rows[1][0]
    last_row = int(cols_rows[1][1])
    usecols = first_col + ":" + last_col
    skiprows = first_row - 1
    nrows = last_row - first_row + 1

    df = pd.read_excel(
        filepath,
        usecols=usecols,
        skiprows=skiprows,
        nrows=nrows,
        engine="openpyxl",
    )
    return df
