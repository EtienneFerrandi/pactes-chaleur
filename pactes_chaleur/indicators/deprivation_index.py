"""
indice_defavorisation.py
Script to get the INSERM's deprivation index (FDep) for each French commune of the 2022 INSEE referential.
"""

import pandas as pd
import numpy as np
import scipy.stats as stats


def compute_nearest_commune_index_mean(df_communes_fdep: pd.DataFrame) -> pd.DataFrame:
    # Calculation of the average of the fdep15 index of the communes closest to Sannerville
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Sannerville", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Touffréville")
            | (df_communes_fdep["code_commune_insee"] == "14215")
            | (df_communes_fdep["libelle_commune_insee"] == "Démouville")
            | (df_communes_fdep["libelle_commune_insee"] == "Émiéville")
            | (df_communes_fdep["libelle_commune_insee"] == "Escoville")
            | (df_communes_fdep["libelle_commune_insee"] == "Saint-Pair")
            | (df_communes_fdep["libelle_commune_insee"] == "Troarn")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Les Trois Lacs
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Les Trois Lacs", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Villers-sur-le-Roule")
            | (df_communes_fdep["libelle_commune_insee"] == "Muids")
            | (df_communes_fdep["libelle_commune_insee"] == "Bouafles")
            | (df_communes_fdep["libelle_commune_insee"] == "Courcelles-sur-Seine")
            | (df_communes_fdep["libelle_commune_insee"] == "Vézillon")
            | (df_communes_fdep["libelle_commune_insee"] == "La Roquette")
            | (df_communes_fdep["libelle_commune_insee"] == "Le Thuit")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Trébons-de-Luchon
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Trébons-de-Luchon", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Benque-Dessous-et-Dessus")
            | (df_communes_fdep["libelle_commune_insee"] == "Saint-Aventin")
            | (df_communes_fdep["libelle_commune_insee"] == "Saccourvielle")
            | (df_communes_fdep["libelle_commune_insee"] == "Castillon-de-Larboust")
            | (df_communes_fdep["libelle_commune_insee"] == "Bagnères-de-Luchon")
            | (df_communes_fdep["libelle_commune_insee"] == "Moustajon")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Beaumont-en-Verdunois
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Beaumont-en-Verdunois", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Ville-devant-Chaumont")
            | (df_communes_fdep["libelle_commune_insee"] == "Moirey-Flabas-Crépion")
            | (df_communes_fdep["libelle_commune_insee"] == "Ornes")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Bezonvaux
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Bezonvaux", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Dieppe-sous-Douaumont")
            | (df_communes_fdep["libelle_commune_insee"] == "Douaumont-Vaux")
            | (df_communes_fdep["libelle_commune_insee"] == "Ornes")
            | (df_communes_fdep["libelle_commune_insee"] == "Maucourt-sur-Orne")
            | (df_communes_fdep["libelle_commune_insee"] == "Mogeville")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Cumières-le-Mort-Homme
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Cumières-le-Mort-Homme", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Chattancourt")
            | (df_communes_fdep["libelle_commune_insee"] == "Marre")
            | (df_communes_fdep["libelle_commune_insee"] == "Champneuville")
            | (df_communes_fdep["libelle_commune_insee"] == "Regnéville-sur-Meuse")
            | (df_communes_fdep["libelle_commune_insee"] == "Béthincourt")
            | (df_communes_fdep["libelle_commune_insee"] == "Forges-sur-Meuse")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Fleury-devant-Douaumont
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Fleury-devant-Douaumont", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Douaumont-Vaux")
            | (df_communes_fdep["libelle_commune_insee"] == "Eix")
            | (df_communes_fdep["libelle_commune_insee"] == "Moulainville")
            | (df_communes_fdep["libelle_commune_insee"] == "Damloup")
            | (df_communes_fdep["libelle_commune_insee"] == "Belleville-sur-Meuse")
            | (df_communes_fdep["libelle_commune_insee"] == "Bras-sur-Meuse")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Haumont-près-Samogneux
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Haumont-près-Samogneux", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Samogneux")
            | (df_communes_fdep["libelle_commune_insee"] == "Brabant-sur-Meuse")
            | (df_communes_fdep["libelle_commune_insee"] == "Moirey-Flabas-Crépion")
            | (df_communes_fdep["libelle_commune_insee"] == "Consenvoye")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Louvemont-Côte-du-Poivre
    df_communes_fdep.loc[df_communes_fdep["libelle_commune_insee"] == "Louvemont-Côte-du-Poivre", "fdep15"] = round(
        df_communes_fdep[
            (df_communes_fdep["libelle_commune_insee"] == "Douaumont-Vaux")
            | (df_communes_fdep["libelle_commune_insee"] == "Vacherauville")
            | (df_communes_fdep["libelle_commune_insee"] == "Bras-sur-Meuse")
            | (df_communes_fdep["libelle_commune_insee"] == "Ornes")
        ]["fdep15"].mean(),
        6,
    )

    return df_communes_fdep


def add_deprivation_index():
    # Load data
    df_communes = pd.read_csv("./data/intermediate/comm_arr_hors_dom_2022.csv")
    df_fdep = pd.read_csv("./data/raw/indice_de_defavorisation/INSERM_2015/FDep_2015.txt", sep=";")

    # Preprocess
    df_fdep["fdep15"] = df_fdep["fdep15"].str.replace(",", ".").astype(float)

    # Merge fdep and communes
    df_communes_fdep = pd.merge(df_fdep, df_communes, left_on=["CODGEO"], right_on=["code_commune_insee"], how="right")

    # Re-organization of dataset
    df_communes_fdep = df_communes_fdep[["CODGEO", "code_commune_insee", "libelle_commune_insee", "P15_POP", "fdep15", "Q5"]]

    # Create a new variable to compare two code INSEE (CODGEO and code_commune_insee)
    # 0 : CODGEO and code_commune_insee are differents
    # 1 : CODGEO et code_commune_insee are the same
    df_communes_fdep["is_equal"] = np.where(df_communes_fdep["CODGEO"] == df_communes_fdep["code_commune_insee"], 1, 0)

    # Replace remaining missing fdep values
    df_communes_fdep = compute_nearest_commune_index_mean(df_communes_fdep)

    # Calculation of z_score_fdep15
    df_communes_fdep["z_score_fdep15"] = stats.zscore(df_communes_fdep["fdep15"])

    # Save
    df_communes_fdep[["code_commune_insee", "libelle_commune_insee", "fdep15", "z_score_fdep15"]].to_csv(
        "./data/processed/comm_arr_hors_dom_2022_fdep15.csv", index=False
    )


if __name__ == "__main__":
    add_deprivation_index()
