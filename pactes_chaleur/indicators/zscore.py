import pandas as pd
import scipy.stats as stats
from typing import List


def compute_z_scores(columns: List[str]):
    z_scores_dfs = []
    for col in columns:
        filepath = f"./data/processed/comm_arr_hors_dom_2022_{col}.csv"
        df = pd.read_csv(filepath)
        z_scores_dfs.append(pd.DataFrame({col: df[col], f"z_score_{col}": stats.zscore(df[col])}))
    final_df = pd.concat([df[["code_commune_insee", "libelle_commune_insee"]]] + z_scores_dfs, axis="columns")
    final_df.to_csv("./data/processed/comm_arr_hors_dom_2022_all_indicators.csv", index=False)
    return final_df


if __name__ == "__main__":
    compute_z_scores(["fdep15", "temperature_moyenne_ete_C"])
