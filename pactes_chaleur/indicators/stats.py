import pandas as pd


if __name__ == "__main__":
    df = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_all_indicators.csv")
    df.describe().to_csv("./data/processed/stats.csv")
    print(df.describe())
