"""
temperature_map.py
"""

import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt


def create_temperature_map():
    # Load data
    df_communes = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_temperature_moyenne_ete_C.csv")
    gdf_communes = gpd.read_file("./data/raw/temperature/communes-20220101-shp/communes-20220101.shp")

    # Merge
    df_merge = pd.merge(left=gdf_communes, right=df_communes, left_on="nom", right_on="libelle_commune_insee", how="left")

    # Create the map
    _, ax = plt.subplots(figsize=(10, 10))

    # Plot the choropleth map
    df_merge.plot(column="temperature_moy_estivale_2022", cmap="YlOrRd", legend=True, ax=ax)

    # Add a title
    ax.set_title("Temperature map of metropolitan French communities (2022)")

    # Set the x and y limits to zoom in on the map
    ax.set_xlim([-5, 10])
    ax.set_ylim([41, 52])

    # Save the map
    plt.savefig("./figures/temperature_map.png")
    plt.show()


if __name__ == "__main__":
    create_temperature_map()
