"""
deprivation_index_map.py
"""

import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt


def create_deprivation_index_map():
    # Load data
    df_communes = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_fdep15.csv")
    gdf_communes = gpd.read_file("./data/raw/indice_de_defavorisation/communes-20220101-shp/communes-20220101.shp")

    # Merge
    df_merge = pd.merge(left=gdf_communes, right=df_communes, left_on="nom", right_on="libelle_commune_insee", how="left")

    # Create the map
    fig, ax = plt.subplots(figsize=(8, 8))

    # Plot the choropleth map
    df_merge.plot(column="fdep15", cmap="YlGn", legend=True, ax=ax)

    # Add a title
    ax.set_title("Deprivation index of metropolitan French communities (2022)")

    # Set the x and y limits to zoom in on the map
    ax.set_xlim([-5, 10])
    ax.set_ylim([40, 55])

    # Save the map
    plt.savefig("./figures/deprivation_index_map.png")
    plt.show()


if __name__ == "__main__":
    create_deprivation_index_map()
