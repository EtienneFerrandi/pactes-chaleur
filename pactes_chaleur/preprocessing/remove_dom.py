"""
remove_dom.py
Remove all French DOM (INSEE code starting with 97).
"""

from pactes_chaleur.data_loading.load_excel import read_excel


def remove_dom(source_file: str, dest_file: str, source_range: str):
    df_communes = read_excel(source_file, range=source_range)

    # Remove DOM
    dom_mask = df_communes["Code"].astype(str).str.startswith("97")
    df_communes.drop(df_communes[dom_mask].index, inplace=True)

    # Rename column
    df_communes.rename({"Libellé": "libelle_commune_insee", "Code": "code_commune_insee"}, axis=1, inplace=True)

    # Save
    df_communes.to_csv(dest_file, index=False)


if __name__ == "__main__":
    source_file = "./data/raw/comm_arr_2022.xlsx"
    dest_file = "./data/intermediate/comm_arr_hors_dom_2022.csv"
    source_range = "A3:B35000"
    remove_dom(source_file, dest_file, source_range)
