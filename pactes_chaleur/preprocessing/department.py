"""
department.py
Script to add department's number for each French commune.

The department's number is the first two number of the postal code, 
not the INSEE department's code.
This is needed to get the latitude and longitude of a French commune.
"""

import pandas as pd
from pactes_chaleur.data_loading.load_excel import read_excel


def add_department(source_file, dest_file, source_range=None):
    if source_range:  # Excel file, in data/raw
        df_communes = read_excel(source_file, range=source_range)
        df_communes.rename({"Libellé": "libelle_commune_insee", "Code": "code_commune_insee"}, axis=1, inplace=True)
    else:  # CSV file, in data/intermediate
        df_communes = pd.read_csv(source_file)

    # Add department code
    df_communes["num_departement"] = df_communes["code_commune_insee"].apply(lambda code_commune: code_commune[:2])
    df_communes["num_departement"] = df_communes["num_departement"].replace({"2A": "20", "2B": "20"})

    # Reorder column
    cols = ["num_departement", "code_commune_insee", "libelle_commune_insee"]
    df_communes = df_communes[cols]

    # Save
    df_communes.to_csv(dest_file, index=False)


if __name__ == "__main__":
    raw_dir = "./data/raw/"
    intermediate_dir = "./data/intermediate/"
    # add_department(raw_dir + "comm_2022.xlsx", intermediate_dir + "comm_dept_2022.csv", "A3:B34958")
    # add_department(raw_dir + "comm_arr_2022.xlsx", intermediate_dir + "comm_arr_dept_2022.csv", "A3:B35000")
    add_department(intermediate_dir + "comm_arr_hors_dom_2022.csv", intermediate_dir + "comm_arr_hors_dom_dept_2022.csv")
