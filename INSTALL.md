# Installation Guide

- [Data Collection](#data-collection)
- [Dependencies](#dependencies)
- [Development](#development)
- [Production](#production)

## Data Collection

> TODO / Indicate how to collect the data necessary for this project :
>- Where and how to get the data ?
>- Where and how to integrate the data in the repository ? (example : in the directory data/raw)


## Dependencies

- **Python version**: 3.10.11
- **Libraries and packages**: listed in [`pyproject.toml`](./pyproject.toml)
- **Virtual environment**: [`pyenv`](https://github.com/pyenv/pyenv)
- **Dependency manager**: [`poetry`](https://python-poetry.org/) 

### Setup virtual environment

#### 1. Prerequisites
Install the [prerequisites](/PREREQUISITES.md) if you have not already done so.

#### 2. Clone this repository 

1. Fork the repository.
2. In a command prompt of your choice, clone your fork.
3. Go in the project folder.

#### 3. Setting up the environment

Doing the following step will enable your local environement to be aligned with the one of any other collaborator.

__3.1 Create a virtual environment__

In a command prompt (use Git bash in Windows):
1. Check existing Python versions with `pyenv`
    ```bash
    pyenv versions
    ```
2. If Python 3.10.11 does not exist, install it
    ```bash
    pyenv install 3.10.11
    ```
3. Set Python 3.10.11 for the local project
    ```bash
    pyenv local 3.10.11
    ```
4. Using `virtualenv`, create a virtual environment named ".env_pactes_chaleur"
    ```bash
    python -m venv .env_pactes_chaleur
    ```
    
5. Activate the virtual environment
    ```bash
    source .env_pactes_chaleur/bin/activate # zsh, bash, etc.
    . .env_pactes_chaleur/Scripts/activate # Git bash
    .\.env_pactes_chaleur\Scripts\activate # Windows PowerShell
    ```

__3.2 Install packages with `poetry`__

Now we need a tool to manage dependencies. Let's use `poetry`.

1. Install `poetry`
    ```bash
    pip install poetry
    ```

2. Install all dependencies
    ```bash
    poetry install
    ```

When you need to install a new package (e.g. nltk), run
```bash
poetry add nltk
```
:warning: Then __commit the files `poetry.lock` and `pyproject.toml`__ to ensure that the package versions are consistent for everyone working on the project.

## Development

> TODO / Indicate how to run the solution in development mode locally.

## Production

> TODO / Indicate, if it exist, a documentation to run the solution in production mode.