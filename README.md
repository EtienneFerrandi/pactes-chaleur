# Challenge GD4H - PACTES chaleur
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Elle organise un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées.

Liens : 
<a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Site</a> / 
<a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Forum</a>

---
**Table des matières :**
- [PACTES chaleur](#pactes-chaleur)
- [Structure du répertoire](#structure-du-répertoire)
- [Documentation](#documentation)
    - [Installation](#installation)
    - [Utilisation](#utilisation)
    - [Contributions](#contributions)
    - [Licence](#licence)
- [Equipe](#equipe)

## PACTES chaleur

Un professionnel de santé qui s’installe ne connaît pas les déterminants environnementaux et de santé de son territoire, et ne peut donc pas anticiper les risques de ses patients.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=35" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Structure du répertoire**

```
- data/ ----------------------------- données brutes et finales pour AtlaSanté
        README.md ------------------- sources de données et le référentiel des communes INSEE (2022)
        intermediate/ --------------- fichiers intermédiaires crés à partir des données brutes
        processed/ ------------------ fichiers finaux pour AtlaSanté
        raw/ ------------------------ données brutes
- figures/ -------------------------- figures (e.g. carte des températures)
- pactes_chaleur/ ------------------- code source 
        data_loading/ --------------- fonctions pour lire les données
        indicators/ ----------------- scripts pour chaque indicateur
        preprocessing/ -------------- ajout des départements, latitude, longitude
- tests/ ---------------------------- fichiers tests
- .gitignore ------------------------ fichiers et répertoires que git ignore
- .python-version ------------------- version de python 
- CONTRIBUTING.md ------------------- lignes directrices pour les contributions
- INSTALL.md ------------------------ guide d'installation des dépendances etc.
- PREREQUISITES.md ------------------ prérequis à installer avant INSTALL.md
- README.en.md ---------------------- README du projet (anglais)
- README.md ------------------------- README du projet (fichier actuel)
- licence.MIT + licence.etalab-2.0 -- licence
- poetry.lock + pyproject.toml ------ fichiers poetry pour installer les dépendences
```

## **Documentation**

>TODO / **Description Solution**

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

>TODO / **documentation d'utilisation de la solution**

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).


## **Equipe**

### Porteurs du projet

- Dr. François Carbonnel
- Dr. Grégoire Mercier
- Paul Bassobert
- Karolina Griffiths
- Mirelle Abraham

### Bénévoles

- Elise Chin
- Emilie Agrech
- Camille Debrock
- Layana Caroupaye-Caroupin
- Manal Ahikki
- Imen Kadri